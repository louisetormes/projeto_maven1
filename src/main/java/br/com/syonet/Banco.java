package br.com.syonet;

public class Banco {

	public BancoEnum banco;
	public int codigo;
	
	public Banco(BancoEnum banco, int codigo) {
		this.banco = banco;
		this.codigo = codigo;
	}
	
	public BancoEnum getBanco() {
		return banco;
	}
	
	public void setBanco(BancoEnum banco) {
		this.banco = banco;
	}
	
	public int getCodigo() {
		return codigo;
	}
	
	public void setCodigo(int codigo) {
		this.codigo = codigo;
	}
	
	public String toString() {
		return "Dados da conta: \n" +
				getCodigo() + " " +
				getBanco();
	}
	
}
