package br.com.syonet;

public class Conta extends Banco{

	
	public int agencia;
	public int numConta;
	public double saldo;
	public TipoContaEnum tipoConta;
	public String titularNome;
	public String titularCpf;
	
	public Conta(int agencia, int numConta, double saldo, TipoContaEnum tipoConta, 
			BancoEnum banco, int codigo, String titularNome, String titularCpf) {
		super(banco, codigo);
		this.agencia = agencia;
		this.numConta = numConta;
		this.saldo = saldo;
		this.tipoConta = tipoConta;
		this.titularNome = titularNome;
		this.titularCpf = titularCpf;
		
	}
	
	public int getAgencia() {
		return agencia;
	}
	
	public void setAgencia(int agencia) {
		this.agencia = agencia;
	}
	
	public int getNumConta() {
		return numConta;
	}
	
	public void setNumConta(int numConta) {
		this.numConta = numConta;
	}
	
	public double getSaldo() {
		return saldo;
	}
	
	public void setSaldo(double saldo) {
		this.saldo = saldo;
	}
	
	public TipoContaEnum getTipoConta() {
		return tipoConta;
	}
	
	public void setTipoConta(TipoContaEnum tipoConta) {
		this.tipoConta = tipoConta;
	}
	
	public String getTitularNome() {
		return titularNome;
	}
	
	public void setTitularNome(String titularNome) {
		this.titularNome = titularNome;
	}
	
	public String getTitularCpf() {
		return titularCpf;
	}
	
	public void setTitularCpf(String titularCpf) {
		this.titularCpf = titularCpf;
	}

	public String toString() {
		return "Dados da conta: \n" +
				getTipoConta() +  " " +
				getAgencia() +  " " +
				getNumConta() +  " " +
				getSaldo();
	}
	}