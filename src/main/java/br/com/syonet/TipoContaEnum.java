package br.com.syonet;

public enum TipoContaEnum {

	CORRENTE("Corrente"),
	POUPANCA("Poupan�a"),
	SALARIO("Sal�rio");
	
	public String tipoConta;
	
	TipoContaEnum(String tipoConta) {
		this.tipoConta = tipoConta;
	}
	
	public String getTipoConta() {
		return tipoConta;
	}
}
