package br.com.syonet;

public enum TitularEnum {

	NOME("Nome"),
	CPF("CPF");
	
	public String titular;
	
	TitularEnum(String titular) {
		this.titular = titular;
	}
	
	public String getTitular() {
		return titular;
	}
}
