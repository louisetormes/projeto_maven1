package br.com.syonet;

import java.util.Arrays;
import java.util.List;
import br.com.syonet.ContaNaoEncontradaException;

public class Main {
	
	public static List<Conta> contas;
	
	public static void main(String[] args) throws ContaNaoEncontradaException, SaldoInsuficienteException {
		
		contas = Arrays.asList(
				new Conta(1, 12345, 50.00, TipoContaEnum.CORRENTE, BancoEnum.BRADESCO, 03, "Louise Saldanha Tormes", "030.388.480-00"),
				new Conta(2, 12121, 1000.00, TipoContaEnum.SALARIO, BancoEnum.CAIXA, 04, "Lucas Reis", "027.600.270-90"),
				new Conta(3, 11111, 10000.00, TipoContaEnum.POUPANCA, BancoEnum.ITAU, 05, "Pedro Lucas Tormes dos Reis", "654.954.030-53"),
				new Conta(5, 33333, 500000.00, TipoContaEnum.POUPANCA, BancoEnum.NUBANK, 06, "Louise Saldanha Tormes", "030.388.480-99"));
	    System.out.println(retornaConta(""));
		System.out.println(consultaConta(null, 0, 0));
		System.out.println(saqueValor(null, 0, 0, 0));
	}
	
	public static String retornaConta(String titularCpf) throws ContaNaoEncontradaException {
		return contas.stream()
		.filter(c -> c.getTitularCpf() == "030.388.480-00")
		.map(c -> "Contas do titular: \n ------------------\n" + c.getTitularNome() +
				  " Banco: " + c.getBanco() +
				  " Ag�ncia: " + c.getAgencia() +
				  " Tipo Conta: " + c.getTipoConta() +
				  " N� Conta: " +c.getNumConta())
		.findAny()
		.orElseThrow(() -> new ContaNaoEncontradaException("Titular sem conta cadastrada!"));
	
		}
	
	public static String consultaConta(BancoEnum banco, int agencia, int numConta) throws ContaNaoEncontradaException {
		return contas.stream()
		.filter(c -> c.getBanco() == BancoEnum.NUBANK)
		.filter(c -> c.getAgencia() == 5)
		.filter(c -> c.getNumConta() == 33333)
		.map(c -> "Titular da conta informada: \n ------------------\n" + c.getTitularNome() +
				  " Banco: " + c.getBanco() +
				  " Ag�ncia: " + c.getAgencia() +
				  " Tipo Conta: " + c.getTipoConta() +
				  " N� Conta: " +c.getNumConta())
		.findAny()
		.orElseThrow(() -> new ContaNaoEncontradaException("Titular sem conta cadastrada!"));
	
		
	}
	
	public static double saqueValor(BancoEnum banco, int agencia, int numConta, double valorSaque) throws SaldoInsuficienteException {
		Conta contas1 = contas.stream()
		.filter(c -> c.getBanco() == BancoEnum.ITAU)
		.filter(c -> c.getAgencia() == 3)
		.filter(c -> c.getNumConta() == 11111)
		.findAny().get();
		if(contas1.getSaldo() != 0 && contas1.getSaldo() >= valorSaque) {
			contas1.setSaldo(contas1.getSaldo() - valorSaque);
			return contas1.getSaldo();
		} if(contas1.getSaldo() != 0 && contas1.getSaldo() < valorSaque) {
			throw new SaldoInsuficienteException("Saldo Insuficiente para saque!");
		}
		return 0;
	}
}
		


