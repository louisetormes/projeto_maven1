package br.com.syonet;

public enum BancoEnum {

	SICREDI("Sicredi"),
	ITAU("Itau"),
	NUBANK("Nubank"),
	BRADESCO("Bradeco"),
	CAIXA("Caixa Econ�mica Federal");
	
	public String banco;
	
	BancoEnum(String banco) {
		this.banco = banco;
	}
	
	public String getBanco() {
		return banco;
	}
}
